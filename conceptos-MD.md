# titulo h1
## titulo h2
### titulo h3 
#### titulo h4
##### titulo h5

1. item 1
2. item 2

- item 3
- item 4
* item 5
* item 6

`este es un bloque de texto encerrado`
 **es negrilla**

| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text  |

links referenciales 

[click aqui chetsheet 1](https://www.markdownguide.org/cheat-sheet/)

[click aqui cheatsheet 2](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)