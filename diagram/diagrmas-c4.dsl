workspace {

    model {
        printerUser = person "Usuario de Impresiones" "Usuario que utiliza el servicio de impresiones, paga las impresiones, administra sus documentos."
        adminUser = person "Usuario Administrador del Sistema" "Usuario que administra lo establecimientos, las peticiones de impresión."
        adminUserPrinter = person "Usuario administrador del Centro de Impresión" "Usuario que administra uno o varios establecimientos."
        systemPrinterDoc = softwareSystem "Sistema de Control e Impresión de Documentos" "Sistema que permite a usuarios imprimir documentos a las impresoras ubicadas en diferentes lugares, y pagar por las impresiones realizadas." {
        
            mobileRender = container "React Móvil" "Proporciona un conjunto de funciones para todos los usuarios dependiendo su Rol, disponible para S.O Android y iOS." "React Native" {
                printerUser -> this "Solicita Impresión, controla documentos."
                apiComponent = component "Manejo de Documentos." "use" "REST API"
                securityComp = component "Manejo de Impresión." "use"
            }
            
            webRender = container "React Web" "Proporciona toda la funcionalidad del Sistema por Internet a los Usuarios por medio de un Navegador de Internet." "React" {
                printerUser -> this "Solicita Impresión, controla documentos."
                adminUser -> this "Administra los establecimientos."
                adminUserPrinter -> this "Administra el Centro de Impresión."
                GestiondePG = component "Gestión de pagos." "use" "REST API"
                ManejosdeDC = component "Manejo de Documentos." "use"
                AdminGN = component "Administración General." "use"
                ControlPrinter = component "Control de Impresion del establecimientos." "use"
                tags "Webrender"
            }
            
            wsPagos = container "Web Servie Boton de Pagos" "Operaciones de pago, historico de transaccion." "Lambda AWS" {
                printerUser -> this "Solicita Impresión, controla documentos."
                adminUser -> this "Administra los establecimientos."
                adminUserPrinter -> this "Administra el centro de impresión."
                
                ProDebito = component "Procesador de pagos Tarjeta de debito" "use"
                ProCredito = component "Procesador de pagos Tarjeta de credito" "use"
                RevoTransaccion = component "Revocacion de Transaccion" "use"
                HistPago = component "Historial de Pagos" "use"
                tags wsPagos
            }
            
            dataBase1 = container "Base de Datos Postgress" "Almacena los usuarios, documentos, impresiones, pagos, establecimientos."{
            mobileRender -> this "Lee y escribir"
            webRender -> this "Lee y escribir"
            tags "DataBase1" 
            }
            
            dataBase2 = container "Base de Datos SQLLITE" "Almacena las  solicitudes de impresión." {
            mobileRender -> this "Lee y escribir"
            tags "DataBase2" 
            }
        }
        
        ServiciePay = softwareSystem "Servicio de Pago" "Servicio de tarjetas de créditos o débito para efectuar el pago."
        
        
        ##RELACIONES##
        
        #RELACIÓN: USUARIOS > SERVICIOS DE PAGOS
        systemPrinterDoc -> ServiciePay "Solicito cobro"
        mobileRender -> ServiciePay "Solicito cobro"

        #RELACIÓN: USUARIOS > COMPONENTES REACT MÓVIL
        printerUser -> apiComponent "Usa"
        printerUser -> securityComp "Usa"
        
        #RELACIÓN: COMPONENTES REACT MÓVIL > BASES DE DATOS POSTGRESS
        apiComponent -> dataBase1 "Lee y escribir"
        securityComp -> dataBase1 "Lee y escribir"

        #RELACIÓN: COMPONENTES REACT MÓVIL > BASES DE DATOS SQLLITE
        securityComp -> dataBase2 "Lee y escribir"
        apiComponent -> dataBase2 "Lee y escribir"
        
        #RELACIÓN: USUARIOS > COMPONENTES REACT WEB
        printerUser -> GestiondePG "Usa"
        printerUser -> ManejosdeDC "Usa"
        adminUser -> AdminGN "Usa"
        adminUserPrinter -> ControlPrinter "Usa"
        
        #RELACIÓN: COMPONENTES REACT WEB > BASES DE DATOS POSTGRESS
        GestiondePG -> dataBase1 "Usa"
        ManejosdeDC -> dataBase1 "Usa"
        AdminGN -> dataBase1 "Usa"
        ControlPrinter -> dataBase1 "Usa"
    }

    views {
        systemContext systemPrinterDoc {
            include *
            autolayout lr
        }

        container systemPrinterDoc {
            include *
            autolayout lr
        }
        
        component mobileRender {
            include *
            autolayout
        }
        
        component webRender {
            include *
            autolayout
        }
        
        component wsPagos {
            include *
            autoLayout
        }
        
        theme default
        
        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
                
            element "Container" {
                background #1061B0
                color #ffffff
            }
            element "dataBase1" {
                shape Cylinder
                background #1061B0
                color #ffffff
            }
            element "dataBase2" {
                shape Cylinder
                background #1061B0
                color #ffffff
            }
            element "Component" {
                background #63BEF2
                color #ffffff
            }
            element "webRender" {
                shape WebBrowser
            }
            element "mobileRender" {
                shape MobileDeviceLandscape
            }
        }
    }

}