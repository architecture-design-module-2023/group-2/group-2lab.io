# Funcionalidad offline 

## Estado
Propuesto

## Contexto
Como usuario registrado en la aplicación, debo tener la posibilidad de trabajar offline en el aplicativo movil para editar los archivos y que
estos se sincronicen cuando este nuevamente en linea.



## Decisión
Se toma la decisión para que es necesario instalar una base de datos en el dispositivo(sql lite).

## Consecuencias 
- Va estar limitado el usuario para completar el proceso de impresion. si no tiene internet
- No se puede tener la seguridad que todos los dispositivos tengan espacio para almacenar localmente.
- Limitar el almacenamiento local y de nube por usuario

## Última fecha de actualización
24 Junio 2024