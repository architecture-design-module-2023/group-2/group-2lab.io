# Servicio de impresión 

## Estado
Propuesto

## Contexto
Como usuario registrado en la aplicación, debo tener la opción de imprimir mis archivos en una impresora local, o en una impresora remota. También debo tener la opción de programar las impresiones de mis archivos para una fecha y hora especificados. Como usuario quiero poder gestionar mis documentos que necesito imprimir. Para ello, debe poder seleccionar el establecimiento, la calidad, el tamaño y otras opciones de impresión. Además, necesito conocer el estado de mis documentos, si ya fueron impresos.

## Decisión
Se toma la decisión para que los usuarios puedan gestionar y controlar sus trabajos de impresión de manera efectiva, brindándoles flexibilidad y control sobre el proceso.

## Consecuencias 
- Mayor flexibilidad y conveniencia.
- Mejor gestión de documentos.
- Personalización de opciones de impresión.

## Última fecha de actualización
24 Junio 2024