# Gestiòn de pagos 

## Estado
Propuesto

## Contexto
Como usuario registrado en la aplicación, debo tener la opción de elegir el metodo de pago que se ajuste a mis 
necesidades, tarjeta de credito, debito, transferencia


## Decisión
Se toma la decisión para que los usuarios puedan elegir el metodo de pago utilizar un boton de pagos el cual no necesite 
inicialmente un pago mensual por su uso para lo cual se sugiere utilizar PAYMENTEZ

## Consecuencias 
- Se puede tener inconvenientes con algunos pagos dependiendo el banco emisor de la tarjeta
- Se debe considerar que tarjetas se van a permitir en base al boton de pagos seleccionado
- Con las transeferencias se puede tener un retraso en la validacion, para lo cual deberia permitirse solo del mismo banco
o tener varias posibilidades de cuentas de transferencias

## Última fecha de actualización
24 Junio 2024