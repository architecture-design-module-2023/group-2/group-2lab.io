# Servicio Control e Impresión de Documentos 

## Estado
Propuesto

## Contexto
Como usuario registrado en la aplicación de SCID, tengo una cuenta en Google Drive vinculada a mi perfil. Cuando accedo a la sección de archivos, debo poder ver una lista de mis archivos almacenados en Google Drive. Cuando selecciono un archivo de la lista para editarlo, debo poder utilizar los servicios proporcionados por Google para editar el archivo. Al realizar los cambios, deben reflejarse correctamente en el archivo almacenado en Google Drive.
Debo poder crear un nuevo archivo, editarlo y guardarlo en mis archivos almacenados en Google Drive. Además, debo poder eliminar mis archivos.
Se requiere tambien darle la faciliad al usuario, de poder programar una impresion, de cualquier documento que este en su almacenamiento, o este editandolo en ese instante.

## Decisión
Se toma la decisión de implementar la integración de Google Drive en la aplicación de SCID que permite a los usuarios servir las capacidades de almacenamiento y edición de archivos de Google, lo que mejora la funcionalidad y la experiencia del usuario en la aplicación; dentro de esto se tendra limitado el horario de impresion, asi como el tipo de impresoras a implementar serian laser minimizando el tema, de largas colas de impresion dentro de un tiempo establecidos por muchos usuarios.

## Consecuencias 
- Sincronización de cambios en tiempo real.
- Brindarles a los usuarios un acceso directo a sus archivos almacenados en la nube.
- Servir las herramientas de edición y colaboración de Google.
- Facilidades de Tiempos de impresion
- Impresion al alcance de muchas persona a la palma de la mano.

## Última fecha de actualización
11 Julio 2023
