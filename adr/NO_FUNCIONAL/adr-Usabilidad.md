# Usabilidad

## Estado
Propuesto

## Contexto
El sistema de servicio de impresion al ser utilizado, por una cantidad extensiva de personas, es necesario que la interfaz se de lo mas amigable intuitiva y facil de usar, Al ser un publico masivo se vuelve necesario tener respuestas claras, sean esto satisfactorias o erroneas.
Esta caracteristicas vienen de la mano, con temas de accesibilidad, como el control de tamaño de letra, contraste de colores entre altos y bajos; asi como alineaciones de parrafo de izquierda a derecha y/o viceversa.

## Decisión
Se toma la decision de contratar, a dos personas especialistas en UI/UX, asi como licencia de framework de desarrollo que apoyara a implementar animaciones visuales de alto impacto.
Se pasara por pruebas exntensivas de confiabilidad de interfaz de usuario.

## Consecuencias 
- Pasar Pruebas de aceptacion de interfaz
- Latencia en tiempo de respuestas
- Procesos visuales, apoyan al usuairo en las pruebas
- Pruebas de accesibilidad graficas al sistema

## Última fecha de actualización
11 Julio 2023
