# Alta Disponibilidad Concurrencia 

## Estado
Propuesto

## Contexto
Esta funcion no permiter reducir la carga  de trabajo  para la  impresion de documentos un parametro de 5 usuario por docuemntos minimo,con varios nodo de cluster,utlizamos software balanceador de carga de documentos y uso de GLB galeria. 

## Decisión
Se toma la decision para reducir la carga de trabajos y sin perdida de datos y escalabilidad para un crecimiento futuro de usuario.
## Consecuencias 
- Posibles recursos limitados.
- Reducion  el tiempo de envio por documentos.

## Última fecha de actualización
11 Julio 2023
