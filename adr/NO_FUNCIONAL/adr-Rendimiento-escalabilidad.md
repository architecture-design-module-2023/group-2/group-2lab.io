# Rendimiento y Escalabilidad

## Status
Propuesto

## Context
El sistema tendrá una rapidez y eficiencia en el procesamiento de solicitudes de los usuarios, ya que se espera un alto volumen de actividades relacionadas con la creación, edición, eliminación, almacenamiento e impresión de documentos.

## Decision
Se utilizará web sockets (Socket IO) para la comunicación real-time, haciendo que el tiempo de respuesta sea casi inmediato.
Balancear aumento de memoria o velocidad de procesamiento en los servidores.
 
## Consequences
El mantenimiento es muy costoso.

## Última fecha de actualización
24 Junio 2023